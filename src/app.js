//建立類別
export class User {
    constructor(fn, ln) { //建構函式，constructor是固定的
        console.log(this);
        this.firstName = fn;
        this.lastName = ln;
        this.email = fn + "_" + ln + "@company.com";

    }
    sayHi() {
        alert(`Hello ${this.firstName}`)
    }
}

export default class Employee extends User {
    constructor(fn, ln, id) {
        super(fn, ln); //指向父類別
        this.id = id;
    }
    // sayHi() {
    //     alert(`Hello ${this.firstName}, id is ${this.id}`)
    // }
    doSomething(todo) {
        alert(`to do ${todo}`)
    }
}


//產生物件
// let userA = new User('Eagle','Wu');
// let userB = new User('Tracy','Chi')
// userA.sayHi();
// userB.sayHi();

// let empA = new Employee("Fiona","Wu","A001");
// empA.sayHi();
// empA.doSomething("teaching");