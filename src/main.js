import {person} from './person.js'    //export的對象沒有加default時，就須加大括號
// import Employee,{User} from './app.js'
import "./main.scss"
import React from 'react';
import ReactDOM from 'react-dom';

const title = <h1>Hello React</h1>;

ReactDOM.render(
    <div>{title}</div>,   //JSX語法
    document.getElementById('div1')
)





// const myDiv = document.querySelector('#div1');
// myDiv.innerHTML = `<h2>${person.firstName} ${person.lastName}</h2>`

// let empA = new Employee('Tom','Wang','A0002');
// empA.sayHi();
// empA.doSomething('Rock n Roll')

// let userA = new User('sf','sfdf')
// userA.sayHi();