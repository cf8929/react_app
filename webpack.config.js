const path = require('path');
const htmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
    entry: './src/main.js',
    output: {
        filename: 'bundle.js',
        path: path.resolve(__dirname, 'dist'), //node js 提供的__dirname代表檔案所在當前路徑


    },
    plugins: [new htmlWebpackPlugin({
        title: 'MFEE02 APP',
        template: './temp.html'
    })],
    devServer: {
        contentBase: path.resolve(__dirname, 'dist'),
        open: true,
        port: 3000,
        compress: true
    },
    module: {
        rules: [
        {
            test: /\.css$/,
            use: ['style-loader', 'css-loader']
        },
        {
            test: /\.scss$/,
            use: ['style-loader', 'css-loader','sass-loader']
        },
        {
            test: /\.(js|jsx)$/, //| or
            exclude: /node_modules/,
            use: ['babel-loader']
        }
    ]
    }
}