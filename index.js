// function Person(){
//     this.age = 0;
//     console.log(this.age);      //0
//     console.log(this);
//     setTimeout(function(){
//         console.log(this);
//        console.log(++this.age);  //NaN  Not a Number
//     },0)
// }

//方法一
// function Person(){
//     var self = this;
//     self.age = 0;
//     console.log(self.age);      //0
//     setTimeout(function(){
//        console.log(++self.age);  //1
//     },0)
// }
//arrow function 讓this一致
// function Person(){

//     this.age = 0;
//     console.log(this.age);  //0
//     setTimeout(()=>{
//         console.log(++this.age);  // NaN
//     },0);
// }
// var p = new Person();

const user = {
    name: "Tom",
    do() {
        console.log(this);
    }
}
user.do(); //this -> user

const _do = user.do //_do = do()
_do(); // window or undefined (use strict)
const __do = user.do.bind(user); //__do = user.do() 綁定user物件
__do(); // user

const names = ["Eric", "Steve", "Jimmy"];
// const ps = names.map(name => "<p>" + name + "</p>");
const ps = names.map(name => `<p>${name}</p>`);

console.log(ps);

let obj = {
    name:"eric",
    age:30,
    email:"eric@gmail.com",
    phone:"02-11122222"
}

//解構賦值
const {email,phone} = obj
console.log(email);
console.log(phone)